<nav class="fh5co-nav" role="navigation">
    <div class="top-menu">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">    
                <div id="fh5co-logo">
                    <a href="{{ url('/') }}">
                    R.E.S.T
                    <span></span></a>
                </div>
                </div>
                <div class="col-xs-10 text-right menu-1">
                    <ul>
                        <li class="active">
                            <a href="{{ url('/') }}">Home</a>
                        </li>
                        <li class="has-dropdown">
                            <a href="#">Menu</a>
                            <ul class="dropdown">
                                <li><a href="#">Sub Menu1</a></li>
                                <li><a href="#">Sub Menu2</a></li>
                                <li><a href="#">Sub Menu3</a></li>
                                <li><a href="#">Sub Menu4</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>